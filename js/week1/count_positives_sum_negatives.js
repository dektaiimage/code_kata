const countPositivesSumNegatives = (list) => {
  
  if (list.length < 1) {
    return [];
  }

  newArrayOne = []
  newArrayTwo = []

  for (var i = 0; i < list.length; i++) {
    if (list[i] > 0) {
      newArrayOne.push(list[i])
    }
    else {
      newArrayTwo.push(list[i])
    }
  }

  sumArrayTwo = 0
  for (var i = 0; i < newArrayTwo.length; i++) {
    sumArrayTwo += newArrayTwo[i]
  }

  newArray = [newArrayOne.length, sumArrayTwo]
  return newArray

}

module.exports = countPositivesSumNegatives
