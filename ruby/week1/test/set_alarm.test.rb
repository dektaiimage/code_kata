require_relative '../set_alarm'

describe 'Set Alarm' do
  it 'Should return false when employed is true and vacation is true' do
    assert_equal set_alarm(true, true), false
  end

  it 'Should return false when employed is false and vacation is true' do
    assert_equal set_alarm(false, true), false
  end

  it 'Should return false when employed is false and vacation is false' do
    assert_equal set_alarm(false, false), false
  end

  it 'Should return true when employed is true and vacation is false' do
    assert_equal set_alarm(true, false), true
  end
end
